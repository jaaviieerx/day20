(function () {
    angular.module('GMSApp')
    .directive('uniqueUCP', ["UCP", function (UCP) {
        return {
        require:'ngModel',
        restrict:'A',
        link:function (scope, el, attrs, ctrl) {
            ctrl.$parsers.push(function (viewValue) {
            if (viewValue) {
                UCP.query({email:viewValue}, function (UCP) {
                if (UCP.length === 0) {
                    ctrl.$setValidity('uniqueUCP', true);
                } else {
                    ctrl.$setValidity('uniqueUCP', false);
                }
                });
                return viewValue;
            }
            });
        }
        };
    }])
})();