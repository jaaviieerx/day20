(function () {
    angular
        .module("GMSApp")
        .controller("GroceryController", GroceryController)
        .controller("EditGroceryCtrl", EditGroceryCtrl)
        .controller("AddGroceryCtrl", AddGroceryCtrl)
        .controller("DeleteGroceryCtrl", DeleteGroceryCtrl);
        
    GroceryController.$inject = ['GMSAppAPI', '$uibModal', '$document', '$scope'];
    EditGroceryCtrl.$inject = ['$uibModalInstance', 'GMSAppAPI', 'items', '$rootScope', '$scope'];
    AddGroceryCtrl.$inject = ['$uibModalInstance', 'GMSAppAPI', 'items', '$rootScope', '$scope'];
    DeleteGroceryCtrl.$inject = ['$uibModalInstance', 'GMSAppAPI', 'items', '$rootScope', '$scope'];
    
    function DeleteGroceryCtrl($uibModalInstance, GMSAppAPI, items, $rootScope, $scope){
        var self = this;
        self.items = items;
        self.deleteGrocery = deleteGrocery;
        console.log(items);
        GMSAppAPI.getGrocery(items).then((result)=>{
            console.log(result.data);
            self.Grocery =  result.data;
        });

        function deleteGrocery(){
            console.log("delete Grocery ...");
            GMSAppAPI.deleteGrocery(self.grocery_list.id).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshGroceryList');
                $uibModalInstance.close(self.run);
            }).catch((error)=>{
                console.log(error);
            });
        }

    }

    function AddGroceryCtrl($uibModalInstance, GMSAppAPI, items, $rootScope, $scope){
        console.log("Add Grocery");
        var self = this;
        self.saveGrocery = saveGrocery;

        self.Grocery = {

        }
        //initializeCalendar($scope);
        function saveGrocery(){
            console.log("save Grocery ...");
            console.log(self.grocery_list.brand);
            console.log(self.grocery_list.name);
            console.log(self.grocery_list.upc12);
            GMSAppAPI.addGrocery(self.grocery_list).then((result)=>{
                console.log(result);
                console.log("Add Grocery -> " + result.id);
                $rootScope.$broadcast('refreshGroceryListFromAdd', result.data);
             }).catch((error)=>{
                console.log(error);
                self.errorMessage = error;
             })
            $uibModalInstance.close(self.run);
        }
    }

    function EditGroceryCtrl($uibModalInstance, GMSAppAPI, items, $rootScope, $scope){
        console.log("Edit Grocery Ctrl");
        var self = this;
        self.items = items;
        //initializeCalendar($scope);

        GMSAppAPI.getGrocery(items).then((result)=>{
           console.log(result.data);
           self.grocery_list =  result.data;
           console.log(self.grocery_list.upc12);
        })

        self.saveGrocery = saveGrocery;

        function saveGrocery(){
            console.log("save Grocery ...");
            console.log(self.grocery_list.brand);
            console.log(self.grocery_list.name);
            console.log(self.grocery_list.upc12);
            GMSAppAPI.updateGrocery(self.grocery_list).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshGroceryList');
             }).catch((error)=>{
                console.log(error);
             })
            $uibModalInstance.close(self.run);
        }

    }

    function GroceryController(GMSAppAPI, $uibModal, $document, $scope) {
        var self = this;
        self.format = "";
        self.grocery = [];
        self.maxsize = 5;
        self.totalItems = 0;
        self.itemsPerPage = 20;
        self.currentPage = 1;

        self.searchGrocery =  searchGrocery;
        self.addGrocery =  addGrocery;
        self.editGrocery = editGrocery;
        self.deleteGrocery = deleteGrocery;
        self.pageChanged = pageChanged;

        function searchAllGrocery(searchKeyword,orderby,itemsPerPage,currentPage){
            GMSAppAPI.searchGrocery(searchKeyword, orderby, itemsPerPage, currentPage).then((results)=>{
                self.grocery_list = results.data.rows;
                self.totalItems = results.data.count;
                $scope.numPages = Math.ceil(self.totalItems/self.itemsPerPage);
            }).catch((error)=>{
                console.log(error);
            });
        }


        function pageChanged(){
            console.log("Page changed " + self.currentPage);
            searchAllGrocery(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
            console.log($scope.numPages);
        }

        $scope.$on("refreshGroceryList",function(){
            console.log("refresh grocery list "+ self.searchKeyword);
            searchAllGrocery(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
        });

        $scope.$on("refreshGroceryListFromAdd",function(event, args){
            console.log("refresh grocery list from id"+ args.id);
            var grocery = [];
            grocery.push(args);
            self.searchKeyword = "";
            self.grocery = grocery;
        });

        function searchGrocery(){
            console.log("search Grocery  ....");
            console.log(self.orderby);
            searchAllGrocery(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
        }

        function addGrocery(size, parentSelector){
            console.log("post add Grocery  ....");
            var items = [];
            var parentElem = parentSelector ? 
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/addGrocery.html',
                controller: 'AddGroceryCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return items;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
            
        }

        function editGrocery(id, size, parentSelector){
            console.log("Edit Grocery...");
            console.log("id > " + id);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/editGrocery.html',
                controller: 'EditGroceryCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return id;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }

        function deleteGrocery(id, size, parentSelector){
            console.log("delete Grocery...");
            console.log("id > " + id);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/deleteGrocery.html',
                controller: 'DeleteGroceryCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return id;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }
    }
})();