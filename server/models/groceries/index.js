module.exports = function(connection, Sequelize){
    var groceries = connection.define('grocerydb', {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        upc12: {
            type: Sequelize.BIGINT(12),
            allowNull: false
        },
        brand: {
            type: Sequelize.STRING,
            allowNull: false
        },
        name:{
            type: Sequelize.STRING,
            allowNull: false
        },
        photourl: {
            type: Sequelize.STRING,
            allowNull: false,
            defaultValue: `http://www.barcodes4.me/barcode/c128a/{grocery_list.upc12}.png?IsTextDrawn=1`
        }
    }, {
        timestamps: false
    });
    return groceries;
}